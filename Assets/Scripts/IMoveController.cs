using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMoveController
{
    public void Move(float horizontalAxis);
}
