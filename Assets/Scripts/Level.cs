using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    [Header("Settings:")]
    [SerializeField] private float _roadLength;
    [SerializeField] private float _minObstacleOffsetZ;
    [SerializeField] private float _maxObstacleOffsetZ;
    [SerializeField] private float _startPlayerOffsetZ;

    [Header("Elements settings")]
    [SerializeField] private float _roadPartLength;
    [SerializeField] private float _roadPartWidth;

    [Header("Prefabs:")]
    [SerializeField] private GameObject _finishPrefab;
    [SerializeField] private GameObject _obstaclePrefab;
    [SerializeField] private GameObject _roadPartPrefab;

    private void Awake()
    {
        Generate();
    }

    [ContextMenu("Generate Road")]
    public void Generate()
    {
        GenerateRoad();
        GenerateObstacle();
    }

    private void GenerateRoad()
    {
        Vector3 localPosition = Vector3.zero;
        int partsCount = Mathf.CeilToInt(_roadLength / _roadPartLength);

        for (int i = 0; i < partsCount; i++)
        {
            GameObject roadPart = Instantiate(_roadPartPrefab, transform);
            roadPart.transform.localPosition = localPosition;

            localPosition.z += _roadPartLength;
        }

        GameObject finishPart = Instantiate(_finishPrefab, transform);
        finishPart.transform.localPosition = localPosition;
    }

    private void GenerateObstacle()
    {
        float roadLength = _roadLength;
        float currentLength = _startPlayerOffsetZ;
        float startX = -_roadPartWidth / 2f;
        float xOffset = _roadPartWidth / 4f;

        while (currentLength < roadLength)
        {
            int intPosition = Random.Range(0, 4);
            float positionX = startX + intPosition * xOffset;

            Vector3 localPosition = new Vector3(positionX, 0f, currentLength);
            GameObject obstacle = Instantiate(_obstaclePrefab, transform);
            obstacle.transform.localPosition = localPosition;

            currentLength += Random.Range(_minObstacleOffsetZ, _maxObstacleOffsetZ);
        }
    }
}
