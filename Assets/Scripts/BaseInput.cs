using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseInput : MonoBehaviour, IInputHandler
{
    private IInputHandler _inputHandler;

    public float GetHorizontalAxis()
    {
        if (_inputHandler == null)
        {
#if UNITY_ANDROID
            _inputHandler = new InputHandler();
#else
            _inputHandler = new KeyboardInputHandler();
#endif
        }

        return _inputHandler.GetHorizontalAxis();
    }

    private void Update()
    {
        if (_inputHandler == null) return;

        (_inputHandler as IUpdatable).UpdateState();
    }
}
