using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    [SerializeField] private Animator _animator;

    private int _runID;

    private void Awake()
    {
        _runID = Animator.StringToHash("Run");
    }

    public void SetRunTrigger()
    {
        SetTrigger(_runID);
    }

    public void SetFallTrigger()
    {
        SetTrigger("Fall");
    }

    public void SetWinTrigger()
    {
        SetTrigger(_runID);
    }

    public void SetTrigger(int id) => _animator.SetTrigger(id);
    public void SetTrigger(string triggerName) => _animator.SetTrigger(triggerName);
    public void SetFloat(float state, string name) => _animator.SetFloat(name, state);
}
