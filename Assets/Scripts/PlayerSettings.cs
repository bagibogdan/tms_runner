using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Player settings", menuName = "Configs/Player settings")]
public class PlayerSettings : ScriptableObject
{
    [SerializeField] private MovementSettings _movementSettings;
    [SerializeField] private List<PlayerUpgrade> _playerUpgrades;

    public MovementSettings MovementSettings => _movementSettings;

    public Level GetLevel(int index)
    {
        return null;
    }
}

[System.Serializable]
public class MovementSettings
{
    [SerializeField] private float _forwardSpeed;
    [SerializeField] private float _roadWidth;

    public float ForwardSpeed => _forwardSpeed;
    public float RoadWidth => _roadWidth;
}

[System.Serializable]
public class PlayerUpgrade
{
    [SerializeField] private float _multiplier;
    [SerializeField] private int _level;
}
