public interface IUpdatable
{
    public void UpdateState();
}
