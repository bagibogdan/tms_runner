using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerController : MonoBehaviour
{
    private static PlayerController _instance;

    public static PlayerController Instance => _instance;

    [SerializeField] private AnimationController _animationController;

    public event Action OnDied;
    public event Action OnFinish;

    private bool _isAlive;
    private IInputHandler _inputHandler;
    private IMoveController _moveController;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartRun();
        }

        if (_isAlive == false) return;

        _moveController.Move(_inputHandler.GetHorizontalAxis());
    }

    private void StartRun()
    {
        _inputHandler = GetComponent<IInputHandler>();
        _moveController = GetComponent<IMoveController>();
        _isAlive = true;
        _animationController.SetRunTrigger();
    }

    private const int _wallsLayerIndex = 6;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.TryGetComponent(out ICollectable collectable))
        {
            collectable.Collect();
        }

        if (collision.gameObject.TryGetComponent(out Obstacle obstacle))
        {
            Died();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent(out ICollectable collectable))
        {
            collectable.Collect();
        }

        if (other.gameObject.TryGetComponent(out Obstacle obstacle))
        {
            Died();
        }

        if (other.gameObject.TryGetComponent(out Finish finish))
        {
            Finish();
        }
    }

    private void Died()
    {
        _isAlive = false;
        _animationController.SetFallTrigger();
        OnDied?.Invoke();
    }

    private void Finish()
    {
        _animationController.SetWinTrigger();
    }
}
