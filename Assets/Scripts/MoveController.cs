using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveController : MonoBehaviour, IMoveController
{
    [SerializeField] private PlayerSettings _playerSettings;
    private MovementSettings _settings;

    public void Move(float horizontalAxis)
    {
        if (_settings == null)
            _settings = _playerSettings.MovementSettings;

        float xOffset = -horizontalAxis * _settings.RoadWidth;
        float zOffset = _settings.ForwardSpeed * Time.deltaTime;

        Vector3 position = transform.position;

        if (Mathf.Abs(position.x + xOffset) > _settings.RoadWidth / 2f)
        {
            xOffset = (_settings.RoadWidth / 2f - Mathf.Abs(position.x)) * Mathf.Sign(position.x);
        }

        position.x += xOffset;
        position.z += zOffset;
        transform.position = position;
    }
}
