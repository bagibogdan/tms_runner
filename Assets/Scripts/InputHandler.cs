using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : IInputHandler, IUpdatable
{
    private bool _isHeld = false;
    private Vector3 _clickPosition;
    private float _horizontalAxis;

    public float GetHorizontalAxis() => _horizontalAxis;

    public void UpdateState()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _isHeld = true;
            _clickPosition = Input.mousePosition;
        }

        if (Input.GetMouseButtonUp(0))
        {
            _isHeld = false;
            _clickPosition = Vector3.zero;
            _horizontalAxis = 0f;
        }

        if (_isHeld)
        {
            Vector3 offset = _clickPosition - Input.mousePosition;
            _horizontalAxis = offset.x / Screen.width;
            _clickPosition = Input.mousePosition;
        }
    }
}
